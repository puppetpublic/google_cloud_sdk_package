# Install the google-cloud-sdk (gcloud, gsutil ..) packages and vault.
#
# $ensure: must be one of present or absent.
# Default: present
#
# In this module, we call google-cloud-sdk as "gcs"
#
# $use_gcs_repo: if true (the default), use packages.cloud.google.com's repository as
#   the source for the gcs packages.
#

# http://packages.cloud.google.com/apt

class google_cloud_sdk_package (
  $ensure           = present,
  $install_kubectl  = false,
){

  if !($ensure in [ 'present', 'absent' ]) {
    fail("ensure must be present or absent, not $ensure")
  }

  if ($::osfamily == 'Debian') {

    file { '/etc/apt/trusted.gpg.d/google-cloud.gpg':
     ensure => absent,
    }

    file { '/etc/apt/sources.list.d/google-cloud-sdk.list':
      ensure  => $ensure,
      content => template('google_cloud_sdk_package/etc/apt/sources.list.d/google-cloud-sdk.list.erb'),
    }

    file {'/usr/local/bin/google_sdk_install':
      ensure     => $ensure,
      mode       => '0755',
      group      => 'root',
      owner      => 'root',
      content    => template('google_cloud_sdk_package/usr/local/bin/google_sdk_install.erb'),
      before     => Exec['google_sdk_install'],
    }

    if ($ensure == 'present') {
      exec { 'google_sdk_install':
        path     => '/bin:/usr/local/bin:/usr/bin:/usr/sbin',
        command  => 'google_sdk_install',
        creates  => '/etc/apt/trusted.gpg.d/google-cloud-keyring.gpg',
        require  => [
                      File["/usr/local/bin/google_sdk_install"],
                      File["/etc/apt/sources.list.d/google-cloud-sdk.list"],
                    ],
        before   => Package['google-cloud-sdk'],
      }
    } else {
      file { '/etc/apt/trusted.gpg.d/google-cloud-keyring.gpg':
       ensure => absent,
      }
    }

   # Install google_sdk_package
    package { 'google-cloud-sdk':
      ensure  => $ensure,
    }

    if ($install_kubectl) {
    # Install kubectl package
      package { 'kubectl':
        ensure  => $ensure,
      }
    } else {
    # uninstall kubectl package
      package { 'kubectl':
        ensure  => absent,
      }
    }

  } else {
    crit("do not yet know how to handle ${::osfamily}")
  }

}
